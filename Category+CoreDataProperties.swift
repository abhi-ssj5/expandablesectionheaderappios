//
//  Category+CoreDataProperties.swift
//  Task5
//
//  Created by Sierra 4 on 14/02/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import Foundation
import CoreData


extension Category {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Category> {
        return NSFetchRequest<Category>(entityName: "Category");
    }

    @NSManaged public var parent: String?
    @NSManaged public var title: String?

}
