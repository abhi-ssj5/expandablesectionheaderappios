//
//  TableViewCell.swift
//  Task5
//
//  Created by Sierra 4 on 14/02/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    var cellUpdate:[String: String]? {
        didSet{
            updateCell()
        }
    }
    
    func updateCell() {
        lblTitle.text = cellUpdate?["name"]
        imgView.layer.cornerRadius = imgView.frame.height / 2
        if (cellUpdate?["type"]?.isEqual("Product"))! {
            lblTitle.text = lblTitle.text! + "\nPrice: " + (cellUpdate?["price"]!)!
            lblTitle.font = lblTitle.font.withSize(14)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
