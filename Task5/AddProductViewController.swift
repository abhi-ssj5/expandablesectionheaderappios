//
//  AddProductViewController.swift
//  Task5
//
//  Created by Sierra 4 on 14/02/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import UIKit
import CoreData

class AddProductViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var txtName: UITextField!
    
    @IBOutlet weak var btnAddImage: UIButton!
    @IBOutlet weak var txtDescription: UITextField!
    @IBOutlet weak var txtPrice: UITextField!

    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var pickerParent: UIPickerView!
    
    var categories:[String] = []
    var parentPicked: String = " "
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtName.delegate = self
        txtDescription.delegate = self
        txtPrice.delegate = self
        
        pickerParent.delegate = self
        pickerParent.dataSource = self
        
        btnAddImage.layer.cornerRadius = btnAddImage.frame.height / 2
        btnSave.layer.cornerRadius = btnSave.frame.height / 2
        
        Core.fetchCategories(&categories)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func backButton(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return categories[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return categories.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag >= 0 || pickerView.tag <= categories.count {
            parentPicked =  categories[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let pickerLabel = UILabel()
        pickerLabel.text = categories[row]
        pickerLabel.font = UIFont(name: pickerLabel.font.fontName, size: 15)
        pickerLabel.textAlignment = NSTextAlignment.center
        return pickerLabel
    }
    
    @IBAction func addProductClicked(_ sender: Any) {
        var proceed: Bool = true
        if (txtName.text?.isEmpty)! || !validateTextField(txtName.text!, field: "Name") {
            Alert.displayAlert("Name is Empty", self)
            proceed = false
            return
        }
        
        if (txtDescription.text?.isEmpty)! {
            Alert.displayAlert("Description is Empty", self)
            proceed = false
            return
        }
        
        if (txtPrice.text?.isEmpty)! || !validatePrice(txtPrice.text!) {
            Alert.displayAlert("Price is Empty", self)
            proceed = false
            return
        }
        
        if parentPicked.isEqual(" ") {
            Alert.displayAlert("Select Parent Category", self)
            proceed = false
            return
        }
        
        if proceed == true {
            Alert.displayAlert("Product Added", self)
            Core.addProduct(name: txtName.text!, parent: parentPicked, desc: txtDescription.text!, price: txtPrice.text!)
        }
    }
    
    func validateTextField(_ value: String, field: String) -> Bool {
        let letters = CharacterSet.letters
        for char in value.unicodeScalars {
            if !letters.contains(char) {
                Alert.displayAlert("Invalid \(field)", self)
                return false
            }
        }
        return true
    }
    
    func validatePrice(_ value: String) -> Bool {
        let numbers = CharacterSet.decimalDigits
        for number in value.unicodeScalars {
            if !numbers.contains(number) {
                Alert.displayAlert("Invalid Price value", self)
                return false
            }
        }
        return true
    }
}
