//
//  ProductPageViewController.swift
//  Task5
//
//  Created by Sierra 4 on 15/02/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import UIKit
import CoreData

class ProductPageViewController: UIViewController {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var txtQuantity: UITextField!
    @IBOutlet weak var btnAddToCart: UIButton!
    
    var nameOfProduct:String?
    var totalPrice:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnAddToCart.layer.cornerRadius = btnAddToCart.frame.height / 2
        var detail:[String: String] = [:]
        Core.fetchProductForDisplay (name: nameOfProduct!, detail: &detail)
        lblName.text = detail["name"]
        lblPrice.text = detail["price"]
        lblDescription.text = detail["desc"]
        txtQuantity.text = "1"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func backButton(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addToCart(_ sender: Any) {
        if (txtQuantity.text?.isEmpty)! || txtQuantity.text == "0" {
            Alert.displayAlert("Enter Quantity", self)
        } else {
            totalPrice = Int(lblPrice.text!)! * Int(txtQuantity.text!)!
            Core.addToCart(name: lblName.text!, price: totalPrice, quantity: Int(txtQuantity.text!)!)
            Alert.displayAlert("Product Added to cart", self)
        }
    }
}
