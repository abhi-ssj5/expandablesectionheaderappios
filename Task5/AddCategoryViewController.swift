//
//  AddCategoryViewController.swift
//  Task5
//
//  Created by Sierra 4 on 13/02/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import UIKit
import CoreData

class AddCategoryViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var pickerParent: UIPickerView!
    
    @IBOutlet weak var btnAddCategory: UIButton!
    var categories:[String] = ["None"]
    var parentPicked: String = "None"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtTitle.delegate = self
        pickerParent.delegate = self
        pickerParent.dataSource = self
        btnAddCategory.layer.cornerRadius = btnAddCategory.frame.height / 2
        
        Core.fetchCategories(&categories)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func backButton(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return categories[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return categories.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag >= 0 || pickerView.tag <= categories.count {
            parentPicked =  categories[row]
            print(parentPicked)
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let pickerLabel = UILabel()
        pickerLabel.text = categories[row]
        pickerLabel.font = UIFont(name: pickerLabel.font.fontName, size: 15)
        pickerLabel.textAlignment = NSTextAlignment.center
        return pickerLabel
    }
    
    @IBAction func addCategory(_ sender: Any) {
        Core.fetchCategories(&categories)
        if !(txtTitle.text?.isEmpty)! &&  !categories.contains(txtTitle.text!) {
            Core.addCategory(title: txtTitle.text!, parent: parentPicked)
            Alert.displayAlert("Category Added", self)
        }
        else {
            Alert.displayAlert("Invalid Category Title", self)
        }
    }
}
