//
//  ViewController.swift
//  Task5
//
//  Created by Sierra 4 on 13/02/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    
    @IBOutlet weak var btnPrevCat: UIButton!
    @IBOutlet weak var btnAddCategory: UIButton!
    @IBOutlet weak var btnAddProduct: UIButton!
    @IBOutlet weak var tableHome: UITableView!
    
    var expand:[Bool] = []
    
    var categoryArray:[[String: String]] = []
    var subCat:[[String: String]] = []
    var tableData:[[[String: String]]] = []
    var prevParent:[String] = ["None"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnPrevCat.layer.cornerRadius = btnPrevCat.frame.height / 2
        loadTableData(parent: "None")
        btnPrevCat.isHidden = true
    }
    
    func loadTableData(parent: String) {
        Core.fetchCategoriesForHome(&categoryArray, parent: parent)
        for cat in categoryArray {
            expand.append(false)
            Core.fetchCategoriesForHome(&subCat, parent: cat["name"]!)
            Core.fetchProductsForHome(&subCat, parent: cat["name"]!)
            print(subCat)
            tableData.append(subCat)
            subCat.removeAll()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        tableData.removeAll()
        categoryArray.removeAll()
        loadTableData(parent: prevParent[prevParent.count - 1])
        dissolveTransition()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func prevCatLoad(_ sender: Any) {
        if prevParent.count > 2 {
            prevParent.removeLast()
            tableData.removeAll()
            categoryArray.removeAll()
            expand.removeAll()
            loadTableData(parent: prevParent[prevParent.count - 1])
            curlDownTransition()
        } else {
            homeButton(self)
        }
    }
    
    @IBAction func homeButton(_ sender: Any) {
        btnPrevCat.isHidden = true
        prevParent.removeAll()
        prevParent.insert("None", at: 0)
        tableData.removeAll()
        categoryArray.removeAll()
        expand.removeAll()
        loadTableData(parent: "None")
        dissolveTransition()
    }
    
    func curlDownTransition() {
        UIView.transition(with: tableHome,
                          duration: 0.50,
                          options: .transitionCurlDown,
                          animations:
            { () -> Void in
                self.tableHome.reloadData()
        },
                          completion: nil);
    }
    
    func curlUpTransition() {
        UIView.transition(with: tableHome,
                          duration: 0.50,
                          options: .transitionCurlUp,
                          animations:
            { () -> Void in
                self.tableHome.reloadData()
        },
                          completion: nil);
    }
    
    func dissolveTransition() {
        UIView.transition(with: tableHome,
                          duration: 0.30,
                          options: .transitionCrossDissolve,
                          animations:
            { () -> Void in
                self.tableHome.reloadData()
        },
                          completion: nil);
    }
    
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return categoryArray.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect(x: 0, y:0, width: self.tableHome.frame.width, height: 50))
        headerView.backgroundColor = UIColor(red: 0.88, green: 0.88, blue: 0.88, alpha: 1.0)
        
        let catImageView = UIImageView(frame: CGRect(x: 10, y:5, width: 40, height: 40))
        catImageView.image = UIImage(named: "ic_developers")
        
        let dropButton = UIButton(frame: CGRect(x: self.tableHome.frame.width - 35 , y:10, width: 25, height: 25))
        
        if expand[section] == false {
            dropButton.setImage(UIImage(named: "ic_keyboard_arrow_up.png"), for: UIControlState.normal)
        } else {
            dropButton.setImage(UIImage(named: "ic_keyboard_arrow_down.png"), for: UIControlState.normal)
        }
        
        let headerTitle = UILabel.init(frame: CGRect(x: 60, y:0, width: self.tableHome.frame.width - 95, height: 50))
        headerTitle.font = headerTitle.font.withSize(14)
        headerTitle.text = categoryArray[section]["name"]
        
        headerView.addSubview(catImageView)
        headerView.addSubview(headerTitle)
        headerView.addSubview(dropButton)
        
        dropButton.tag = section
        dropButton.addTarget(self, action: #selector(self.tapExpandButton(_:)), for: .touchUpInside)
        
        return headerView
    }
    
    func tapExpandButton(_ sender: UIButton) {
        print("header \(sender.tag) clicked")
        if expand[sender.tag] == false {
            expand[sender.tag] = true
        } else {
            expand[sender.tag] = false
        }
        dissolveTransition()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if expand[indexPath.section] == true {
            return 56
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath) as! TableViewCell
        cell.cellUpdate = tableData[indexPath.section][indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var selected = tableData[indexPath.section][indexPath.row]
        
        if (selected["type"]?.isEqual("Product"))! {
            performSegue(withIdentifier: "DisplayProduct", sender: selected["name"])
        } else {
            tableData.removeAll()
            categoryArray.removeAll()
            prevParent.append(selected["name"]!)
            expand.removeAll()
            loadTableData(parent: selected["name"]!)
            curlUpTransition()
            btnPrevCat.isHidden = false
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "DisplayProduct") {
            let DestViewController = segue.destination as! ProductPageViewController
            DestViewController.nameOfProduct = (sender as? String)!
        }
    }
    
}
