//
//  commonFunctions.swift
//  Task5
//
//  Created by Sierra 4 on 14/02/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class Core {
    class func addCategory(title: String, parent: String) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let catEntity  = NSEntityDescription.entity(forEntityName: "Category", in: appDelegate.persistentContainer.viewContext)!
        let category = NSManagedObject(entity: catEntity, insertInto: appDelegate.persistentContainer.viewContext)
        
        category.setValue(title, forKeyPath: "title")
        category.setValue(parent , forKeyPath: "parent")
        
        do {
            try appDelegate.persistentContainer.viewContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    class func addProduct(name: String, parent: String, desc: String, price: String) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let catEntity  = NSEntityDescription.entity(forEntityName: "Products", in: appDelegate.persistentContainer.viewContext)!
        let product = NSManagedObject(entity: catEntity, insertInto: appDelegate.persistentContainer.viewContext)
        
        product.setValue(name, forKeyPath: "title")
        product.setValue(parent , forKeyPath: "parent_id")
        product.setValue(desc , forKeyPath: "desc")
        product.setValue(price , forKeyPath: "price")
        
        do {
            try appDelegate.persistentContainer.viewContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }

    }
    
    class func fetchCategories(_ categories: inout [String]) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Category")
        do {
            let results = try appDelegate.persistentContainer.viewContext.fetch(fetchRequest)
                for task in results {
                    categories.append(task.value(forKeyPath: "title") as! String? ?? "None")
                }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    class func fetchCategoriesForHome(_ categories: inout [[String:String]], parent: String) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Category")
        do {
            let results = try appDelegate.persistentContainer.viewContext.fetch(fetchRequest)
                for task in results {
                    if parent.isEqual(task.value(forKeyPath: "parent") as! String?) {
                        categories.append(["name": (task.value(forKeyPath: "title") as! String?)!, "type": "Category"])
                    }
                }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    class func fetchProductsForHome (_ categories: inout [[String:String]], parent: String) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Products")
        do {
            let results = try appDelegate.persistentContainer.viewContext.fetch(fetchRequest)
                for task in results {
                    if parent.isEqual(task.value(forKeyPath: "parent_id") as! String?) {
                        categories.append(["name": (task.value(forKeyPath: "title") as! String?)!,
                                           "type": "Product",
                                          "price": (task.value(forKeyPath: "price") as? String)!])
                    }
                }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    class func fetchProductForDisplay (name: String, detail: inout [String: String]) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Products")
        do {
            let results = try appDelegate.persistentContainer.viewContext.fetch(fetchRequest)
            for task in results {
                if name.isEqual(task.value(forKeyPath: "title") as! String?) {
                    detail["name"] = task.value(forKeyPath: "title") as? String
                    detail["price"] = task.value(forKeyPath: "price") as? String
                    detail["desc"] = task.value(forKeyPath: "desc") as? String
                }
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    class func addToCart(name: String, price: Int, quantity: Int) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let cartEntity  = NSEntityDescription.entity(forEntityName: "Cart", in: appDelegate.persistentContainer.viewContext)!
        let product = NSManagedObject(entity: cartEntity, insertInto: appDelegate.persistentContainer.viewContext)
        
        product.setValue(name, forKeyPath: "name")
        product.setValue(price , forKeyPath: "price")
        product.setValue(quantity, forKeyPath: "quantity")
        do {
            try appDelegate.persistentContainer.viewContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    class func fetchCartProducts(_ list: inout [[String: String]]) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Cart")
        let context = appDelegate.persistentContainer.viewContext
        do {
            let results = try context.fetch(fetchRequest)
            if results.count > 0 {
                for task in results {
                    list.append(["name": task.value(forKeyPath: "name") as? String ?? "nil",
                     "price": String(task.value(forKeyPath: "price") as? Int32 ?? 0),
                     "quantity": String(task.value(forKeyPath: "quantity") as? Int32 ?? 0)
                     ])
                     print(task.value(forKeyPath: "name") as? String ?? "nil")
                     print(task.value(forKeyPath: "price") as? Int32 ?? 0)
                     print(task.value(forKeyPath: "quantity") as? Int32 ?? 0)
                }
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    class func deleteProductFromCart(index: Int) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Cart")
        let context = appDelegate.persistentContainer.viewContext
        do {
            let results = try context.fetch(fetchRequest)
            let task = results[index]
            context.delete(task)
            do {
                try context.save()
                print("Item deleted")
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
}

class Alert {
    class func displayAlert(_ userMessage: String, _ obj: UIViewController) {
        let alertMessage = UIAlertController(title:"Alert", message: userMessage, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title:"OK", style: UIAlertActionStyle.default, handler: nil)
        alertMessage.addAction(okAction)
        obj.present(alertMessage, animated: true, completion: nil)
    }
}

