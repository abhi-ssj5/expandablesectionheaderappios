//
//  CartViewController.swift
//  Task5
//
//  Created by Sierra 4 on 14/02/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import UIKit

class CartViewController: UIViewController {

    @IBOutlet weak var cartTable: UITableView!
    @IBOutlet weak var lblTotalPrice: UILabel!
    var cartItems:[[String: String]] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        Core.fetchCartProducts(&cartItems)
        calculateSum()
    }

    func calculateSum() {
        var temp:Int = 0
        for item in cartItems {
            temp = temp + Int(item["price"]!)!
        }
        lblTotalPrice.text = " \(temp)"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func backButton(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
}

extension CartViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cartCell = tableView.dequeueReusableCell(withIdentifier: "CartCell", for: indexPath) as! CartTableViewCell
        cartCell.cartCellUpdate = cartItems[indexPath.row]
        cartCell.btnDelete.tag = indexPath.row
        cartCell.btnDelete.addTarget(self, action: #selector(self.deleteProduct), for: .touchUpInside)
        return cartCell
    }
    
    func deleteProduct(sender: UIButton) {
        let selectedItemIndex = sender.tag
        Core.deleteProductFromCart(index: selectedItemIndex)
        print("\(selectedItemIndex) Button Clicked")
        cartItems.removeAll()
        Core.fetchCartProducts(&cartItems)
        self.calculateSum()
        
        UIView.transition(with: cartTable,
                                  duration: 0.50,
                                  options: .transitionFlipFromTop,
                                  animations:
            { () -> Void in
                self.cartTable.reloadData()
        },
                                  completion: nil);
    }
}
