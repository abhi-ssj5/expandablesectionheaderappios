//
//  CartTableViewCell.swift
//  Task5
//
//  Created by Sierra 4 on 15/02/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import UIKit

class CartTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblProductPrice: UILabel!
    @IBOutlet weak var lblQuantity: UILabel!
    
    @IBOutlet weak var btnDelete: UIButton!

    var cartCellUpdate:[String: String]? {
        didSet{
            updateCell()
        }
    }
    
    func updateCell() {
        lblProductName.text = cartCellUpdate?["name"]
        lblProductPrice.text = "Price: " + (cartCellUpdate?["price"])!
        lblQuantity.text = "Quantity: " + (cartCellUpdate?["quantity"])!
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnDelete.layer.cornerRadius = btnDelete.frame.height / 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
