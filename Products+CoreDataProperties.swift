//
//  Products+CoreDataProperties.swift
//  Task5
//
//  Created by Sierra 4 on 14/02/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import Foundation
import CoreData


extension Products {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Products> {
        return NSFetchRequest<Products>(entityName: "Products");
    }

    @NSManaged public var desc: String?
    @NSManaged public var img: String?
    @NSManaged public var parent_id: String?
    @NSManaged public var price: String?
    @NSManaged public var title: String?

}
